import express from "express";
import {
  getClubs,
  getClubById,
  saveClub,
  updateClub,
  deleteClub,
} from "../controllers/ClubController.js";

const router = express.Router();

router.get("/clubs", getClubs);
router.get("/club/:id", getClubById);
router.post("/clubs", saveClub);
router.patch("/club/:id", updateClub);
router.delete("/club/:id", deleteClub);

export default router;
