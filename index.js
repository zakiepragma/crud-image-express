import express from "express";
import FileUpload from "express-fileupload";
import cors from "cors";
import ClubRoute from "./routes/ClubRoute.js";

const app = express();

app.use(cors());
app.use(express.json());
app.use(FileUpload());
app.use(express.static("public"));
app.use(ClubRoute);

app.listen(5000, () => {
  console.log("Server up dan running...");
});
