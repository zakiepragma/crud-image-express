import { Sequelize } from "sequelize";

const db = new Sequelize("crud_club", "root", "password", {
  host: "localhost",
  dialect: "mysql",
});

export default db;
