import Clubs from "../models/ClubModel.js";
import path from "path";
import fs from "fs";
import { where } from "sequelize";

export const getClubs = async (req, res) => {
  try {
    const response = await Clubs.findAll();
    res.json(response);
  } catch (error) {
    console.log(error.message);
  }
};

export const getClubById = async (req, res) => {
  try {
    const response = await Clubs.findOne({
      where: {
        id: req.params.id,
      },
    });
    res.json(response);
  } catch (error) {
    console.log(error.message);
  }
};

export const saveClub = async (req, res) => {
  if (req.files === null)
    return res.status(400).json({ msg: "No File Uploaded" });

  const name = req.body.name;
  const file = req.files.file;
  const fileSize = file.data.length;
  const ext = path.extname(file.name);
  const fileName = file.md5 + ext;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;
  const allowedType = [".png", ".jpg", ".jpeg"];

  if (!allowedType.includes(ext.toLowerCase()))
    return res.status(422).json({ msg: "Invalid image" });

  if (fileSize > 5000000)
    return res.status(422).json({ msg: "Image must be less than 5 mb" });

  file.mv(`./public/images/${fileName}`, async (err) => {
    if (err) return res.status(500).json({ msg: err.message });

    try {
      await Clubs.create({ name: name, image: fileName, url: url });
      res.status(201).json({ msg: "Club Created Successfully" });
    } catch (error) {
      console.log(error.message);
    }
  });
};

export const updateClub = async (req, res) => {
  const club = await Clubs.findOne({
    where: {
      id: req.params.id,
    },
  });

  if (!club) return res.status(404).json({ msg: "No data found" });

  let fileName = "";

  if (req.files === null) {
    fileName = club.image;
  } else {
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    fileName = file.md5 + ext;

    const allowedType = [".png", ".jpg", ".jpeg"];

    if (!allowedType.includes(ext.toLowerCase()))
      return res.status(422).json({ msg: "Invalid image" });

    if (fileSize > 5000000)
      return res.status(422).json({ msg: "Image must be less than 5 mb" });

    const filepath = `./public/images/${club.image}`;
    fs.unlinkSync(filepath);

    file.mv(`./public/images/${fileName}`, (err) => {
      if (err) return res.status(500).json({ msg: err.message });
    });
  }

  const name = req.body.name;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;

  try {
    await Clubs.update(
      {
        name: name,
        image: fileName,
        url: url,
      },
      {
        where: {
          id: req.params.id,
        },
      }
    );
    res.status(200).json({ msg: "Club Updated Successfully" });
  } catch (error) {
    console.log(error.message);
  }
};

export const deleteClub = async (req, res) => {
  const club = await Clubs.findOne({
    where: {
      id: req.params.id,
    },
  });

  if (!club) return res.status(404).json({ msg: "No data found" });

  try {
    const filepath = `./public/images/${club.image}`;
    fs.unlinkSync(filepath);
    await Clubs.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({ msg: "Club Deleted Successfully" });
  } catch (error) {
    console.log(error.message);
  }
};
